/*eslint no-undef: "error"*/
/*eslint-env node*/
module.exports = function( grunt ) {
    grunt.initConfig( {

        // Let's provide our users a minified version of all our CSS files
        cssmin: {
            target: {
                files: [
                    {
                        expand: true,
                        cwd: 'css/',
                        src: [ '*.css', '!*.min.css' ],
                        dest: 'css/',
                        ext: '.min.css'
                    }
                ]
            }
        },

        // This creates a clean WP Plugin copy to use in production
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: [
                            './*.php',
                            './partials/*.php',
                            './includes/*.php',
                            'library/**',
                            './css/**',
                            './scss/**',
                            './includes/acf/**',
                            './README.md',
                            './CHANGELOG.md'
                        ],
                        dest: 'dist/'
                    }
                ]
            }
        },

        // Let's zip up our /dist (production wordpress plugin)
        // Change version number when needed
        compress: {
            main: {
                options: {
                    archive: 'tcu_faculty_staff_posttype.3.1.0.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: [ '**' ],
                        dest: 'tcu_faculty_staff_posttype/'
                    }
                ]
            }
        }
    } );

    grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
    grunt.loadNpmTasks( 'grunt-contrib-copy' );
    grunt.loadNpmTasks( 'grunt-contrib-compress' );

    // min our CSS files
    grunt.registerTask( 'default', [ 'cssmin' ] );

    // Development workflow
    grunt.registerTask( 'build', [ 'copy' ] );

    // Zip our clean directory to easily install in WP
    grunt.registerTask( 'zip', [ 'compress' ] );
};
