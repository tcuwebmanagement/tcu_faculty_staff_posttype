<?php
/**
 * This page displays Faculty & Staff by department term
 * We also display posts by department term's children
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

$tcu_departments = get_terms( array( 'taxonomy' => Tcu_Faculty_Staff_Main::DEPARTMENT ) );

/**
 * The ACF name for the orderby field
 * This option is in the settings page
 */
$tcu_orderby = Tcu_Faculty_Staff_Admin::LEVDEPTORDERBY;

/**
 * The option to order our query by ASC/DESC order
 * This option is located in the settings page
 */
$tcu_order = get_field( Tcu_Faculty_Staff_Admin::LEVDEPTORDER, 'option' );

/**
 * We grab the current department slug so we can link
 * to the department's website
 */
$tcu_current_dept = wp_get_post_terms(
	get_the_ID(), Tcu_Faculty_Staff_Main::DEPARTMENT, array(
		'fields' => 'all',
		'parent' => 0,
	)
);

// If $tcu_current_dept not empty then lets grab the array key.
if ( ! empty( $tcu_current_dept ) ) {
	$tcu_key = key( $tcu_current_dept );
}

// Grab our header.
get_header();

// Echo our breadcrumbs.
if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
} ?>

<div class="tcu-layoutwrap--transparent">

	<div class="tcu-layout-constrain cf">

		<main class="unit size1of1 m-size1of1 main tcu tcu-faculty-staff-content cf" id="main">

			<h1 class="tcu-mar-b0"><?php single_cat_title(); ?></h1>

			<?php if ( ! empty( $tcu_current_dept ) ) { ?>
				<h2 class="h4 tcu-mar-t0"><em>Return to <a href="/<?php echo esc_attr( $tcu_current_dept[ $tcu_key ]->slug ); ?>"><?php single_cat_title(); ?> website</a></em></h2>
			<?php } ?>

			<?php
			/**
			 * Filter through departments and grab the current term
			 * We are going to query through the department term's children
			 */
			$tcu_current_dept_term = array_filter(
				$tcu_departments, function ( WP_Term $post ) {
					if ( single_cat_title( '', false ) === $post->name ) {
						return $post;
					}
				}
			);

			/**
			 * Only start if this page matches the current department term
			 */
			if ( $tcu_current_dept_term ) :

				// Fetch array key.
				$tcu_key = key( $tcu_current_dept_term );

				/**
				 * Find all children of current term by id
				 * Sort by term_id ASC
				 */
				$tcu_term_id = get_terms(
					array(
						'taxonomy' => Tcu_Faculty_Staff_Main::DEPARTMENT,
						'child_of' => $tcu_current_dept_term[ $tcu_key ]->term_id,
						'orderby'  => 'term_id',
						'order'    => 'ASC',
					)
				);

				/**
				 * Current department terms associated with 1 or more posts
				 * Let's make sure we don't have empty tabs
				 */
				$tcu_terms = array_filter(
					$tcu_term_id, function ( $n ) {
						return $n->count >= 1;
					}
				);
			?>

			<!-- Begin our tabs -->
			<div class="tcu-responsive-tabs tcu-top32">

				<ul>
					<?php
					/**
					 * Loop through child terms of $tcu_current_dept_term
					 * This creates our tabs
					 */
					foreach ( $tcu_terms as $term ) :
					?>
						<li><a aria-label="<?php echo esc_html__( 'View Faculty &amp; Staff filtered by ', 'tcu_faculty_staff_posttype' ) . esc_html( $term->name ); ?>" href="#<?php echo esc_attr( $term->slug ); ?>"><?php echo esc_html( $term->name ); ?></a></li>
					<?php endforeach; ?>
				</ul><!-- End of .tcu-tabs-nav -->

				<?php
				/**
				 * Loop through child terms of $tcu_current_dept_term
				 */
				foreach ( $tcu_terms as $term ) :

						/**
						 * Default arguments for WP_Query
						 */
						$tcu_defaults = array(
							'tax_query' => array(
								'relation'          => 'AND',
								'include_children ' => false,
								array(
									'taxonomy' => Tcu_Faculty_Staff_Main::DEPARTMENT,
									'field'    => 'name',
									'terms'    => single_cat_title( '', false ),
								),
								array(
									'taxonomy' => Tcu_Faculty_Staff_Main::DEPARTMENT,
									'field'    => 'slug',
									'terms'    => $term->slug,
								),
							),
						);

						/**
						 * Grab args depending on settings page
						 * Determines the order and orderby of the page
						 *
						 * @return The args of the WP_Query to use for ordering
						 */
						$tcu_args = tcu_orderby_args( $tcu_orderby, $tcu_order, $tcu_defaults );

						// Let's create our unique transient name.
						$tcu_transient = $term->slug . Tcu_Faculty_Staff_Main::TRANSIENTSLUG;

						/**
						 * Start a new WP_Query
						 * with args from settings page
						 *
						 * @return Object The WP_Query object with all our posts
						 */
						$tcu_new_query = tcu_query_posts( $tcu_args, $tcu_transient );

					if ( $tcu_new_query->have_posts() ) :
					?>

						<!-- begin our tab body -->
						<div id="<?php echo esc_attr( $term->slug ); ?>">

							<!-- Let's begin our table -->
							<table class="tcu-table tcu-table--fs tcu-article__content cf">

								<?php
								/**
								 * Start the loop.
								 */
								while ( $tcu_new_query->have_posts() ) :
									$tcu_new_query->the_post();

									/*
										* Include table rows
										*/
									include 'partials/staff-table.php';

									// End the loop.
								endwhile;
								wp_reset_postdata();
								?>

							</table>

						</div><!-- end of our tabs body -->

					<?php
					else :

						// If no content, include the "No posts found" template.
						include 'partials/content-none.php';

					endif;

				endforeach;

			endif;
			?>

			</div><!-- end of .tcu-responsive-tabs -->

		</main><!-- end of .main -->

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
