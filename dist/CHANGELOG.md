# TCU Faculty & Staff

## Author Mayra Perales <mailto:m.j.perales@tcu.edu>

**v3.1.0**

*   Added PHPCS, ESLint, and SassLint
*   Fix linting issues to follow WordPress Coding Standards
*   Removed unnecessary aria roles
*   Added warning notice when the Advance Custom Fields plugin is not installed
*   Added h1 headings for archive templates
*   Fix error for Areas of Study - content not found with special characters in name
*   Removed post_ID from the archive templates - It was causing duplicates of same CSS ID
*   Fix undefined variable error that occurs when the parent theme is installed
*   Fix type in activation hook for a method name
*   Remove search form from templates

**3.0.0**

*   Added sorting options in the settings page
*   Added json files for our ACF fields
*   Moved code into PHP classes
*   Cleaned up a lot of code
*   Added a first name input for sorting

**v2.4.12**

*   Fixed user capabilites

**v2.4.11**

*   Deleted the Auto-update class
*   Added md5 hash to hide update link

**v1.4.11**

*   Fixed typo in department capabilities

**v1.4.10**

*   Fixed errors caused when TCU News is active
*   Added author and keyword to admin table
*   Added keyword filter to admin
*   Added user capabilities

**v1.4.9**

*   Delete transients when faculty/staff post is saved
*   Cleaned PHP code to meet PSR standards

**v1.4.8**

*   Fixed major issue with the transient repeating items

**v1.4.7**

*   Created a transient for the Faculty/Staff Query
*   Transient will cache results for a day

**v1.3.7**

*   Department tab will not display if empty
*   Areas of Study tab will not display if empty
*   Updated HTML for accordion
*   Moved text into \_e()
*   Added pagination to Areas of Study template

**1.3.6**

*   Added Auto-Update class
*   exchanged ACF json files for PHP includes

**1.2.6**

*   removed taxonomy global variable
*   cleaned up a lot of code
*   added pretty permalinks for Areas of Study
*   order Areas of Study by last name

**v1.1.6**

*   fixed order of taxonomy in department template
*   fixed areas of study template to display only parent dept

**v1.1.5**

*   inlined styles
*   removed wp_enqueue_scripts function

**v1.1.4**

*   fixed parent department not showing up correctly
*   fixed parent department for admin section
*   rm padding left on single template for images/top content

**v1.1.3**

*   moved search styles into plugin
*   added conditionals for advance search form
*   min CSS
