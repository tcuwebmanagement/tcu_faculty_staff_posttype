<?php
/**
 * WP_Query for College Office types
 * This loop is located only in archive-faculty_staff.php
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */

/**
 * The ACF name for the orderby field
 * This option is in the settings page
 */
$tcu_orderby = Tcu_Faculty_Staff_Admin::COLLEGETYPESORDERBY;

/**
 * The option to order our query by ASC/DESC order
 * This option is located in the settings page
 */
$tcu_order = get_field( Tcu_Faculty_Staff_Admin::COLLEGETYPEORDER, 'option' );

/**
 * Default arguments for WP_Query
 */
$tcu_defaults = array(
	'tax_query' => array(
		'include_children' => false,
		array(
			'taxonomy' => $office->taxonomy,
			'field'    => 'slug',
			'terms'    => $office->slug,
		),
	),
);

/**
 * Grab args depending on settings page
 * Determines the order and orderby of the page
 *
 * @return array The args of the WP_Query to use for ordering
 */
$tcu_args = tcu_orderby_args( $tcu_orderby, $tcu_order, $tcu_defaults );

// Let's create our unique transient name.
$tcu_transient = $office->slug . Tcu_Faculty_Staff_Main::TRANSIENTSLUG;

/**
 * Start a new WP_Query
 * with args from settings page
 *
 * @return object The WP_Query object with all our posts
 */
$tcu_new_query = tcu_query_posts( $tcu_args, $tcu_transient );

if ( $tcu_new_query->have_posts() ) : ?>

	<h2><?php echo esc_html( $office->name ); ?></h2>

	<!-- Let's begin our table -->
	<table class="tcu-table tcu-table--fs tcu-article__content cf">

	<?php
	/**
	 * Start the loop
	 */
	while ( $tcu_new_query->have_posts() ) :
		$tcu_new_query->the_post();

			include 'staff-table.php';

		// End the loop.
	endwhile;
	wp_reset_postdata();
	?>

	</table><!-- end of our table -->

<?php
else :

	// If no content, include the "No posts found" template.
	include 'content-none.php';

endif;
?>
