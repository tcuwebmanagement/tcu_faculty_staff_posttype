<?php
/**
 * The template for displaying the pagination
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */

if ( function_exists( 'tcu_page_navi' ) ) {
	tcu_page_navi();
} else {
?>
	<nav class="wp-prev-next">
		<ul class="cf">
			<li class="prev-link"><?php next_posts_link( __( '&laquo; Previous Page', 'tcu_faculty_staff_posttype' ) ); ?></li>
			<li class="next-link"><?php previous_posts_link( __( 'Next &raquo;', 'tcu_faculty_staff_posttype' ) ); ?></li>
		</ul>
	</nav>
<?php
} ?>
