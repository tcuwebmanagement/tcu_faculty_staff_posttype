<?php
/**
 * Faculty Staff sidebar
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 1.0.0
 */

?>
<aside class="tcu-sidebar tcu-top24 unit size1of3 m-size1of3 cf">

	<?php if ( is_active_sidebar( 'tcu-fs-sidebar' ) ) : ?>

		<?php dynamic_sidebar( 'tcu-fs-sidebar' ); ?>

	<?php else : ?>

		<?php // This content shows up if there are no widgets defined in the backend. ?>

		<div class="tcu-alert tcu-alert--help">
			<p><?php esc_html_e( 'Please activate some widgets.', 'tcu_faculty_staff_posttype' ); ?></p>
		</div>

	<?php endif; ?>

</aside><!-- end of .sidebar -->
