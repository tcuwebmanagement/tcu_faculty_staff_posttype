# TCU Faculty Staff Post Type Plugin

The TCU Staff Post Type plugin designed to be used with our the TCU Web Standards Parent Theme.

__GETTING STARTED__

We use GRUNT as our task runner. All our WordPress production files are located in dist/ directory. Check out our Gruntfile.js for instructions on how the set up. We use BEM for naming all our classes. We also use a prefix of "tcu" within our CSS classes.


__WordPress Plugin__

Run grunt zip to compress a clean zip version of the distribution directory. From the WordPress admin screen, select Plugins from the menu. Click on Add new plugin and select the zip file you just created to install. Activate the plugin. Make sure you have the TCU Web Standards theme installed before beginning this process.

    dist/

__Working with an existing Grunt project__

Assuming that the Grunt CLI has been installed and that the project has already been configured with a package.json and a Gruntfile, it's very easy to start working with Grunt:

    - Change to the project's root directory.
    - Install project dependencies with npm install.
    - Run Grunt with grunt.

That's really all there is to it. Installed Grunt tasks can be listed by running grunt --help but it's usually a good idea to start with the project's documentation.

__Install NPM__

If you have not used Grunt before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins.

__GRUNT PLUGINS__

Gruntfile.js is ready to use the following:

    - npm install grunt-contrib-cssmin --save-dev
    - npm install grunt-contrib-copy --save-dev
    - npm install grunt-contrib-compress --save-dev


Developed by **Mayra Perales**: <m.j.perales@tcu.edu>

## Submit Bugs & or Fixes:

* [https://TCUWebmanage@bitbucket.org/TCUWebmanage/tcu_faculty_staff_posttype.git](https://TCUWebmanage@bitbucket.org/TCUWebmanage/tcu_faculty_staff_posttype.git)

## Meta
* [Changelog](CHANGELOG.md)
