<?php
/**
 * Takes care of our UI within the WordPress dashboard
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 *
 * @deprecated See file class-tcu-faculty-staff-admin.php
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Register CPT, widgets, update edit.php, and generate ACF fields
 *
 * @deprecated Since 3.0.0 No longer used by internal code and not recommended.
 */
class Tcu_Faculty_Staff_Admin {

	// Display Tabs.
	const DEPARTMENTTAB   = 'display_department_tab';
	const AREASOFSTUDYTAB = 'display_areas_of_study_tab';

	// College level orderby.
	const AREASOFSTUDYORDERBY = 'areas_sort_each_faculty_&_staff_by';
	const AREASOFSTUDYORDER   = 'areas_ascending_descending';
	const DEPARTMENTORDERBY   = 'department_sort_each_faculty_&_staff_by';
	const DEPARTMENTORDER     = 'department_areas_ascending_descending';
	const COLLEGETYPESORDERBY = 'college_types_sort_each_faculty_&_staff_by';
	const COLLEGETYPEORDER    = 'college_types_ascending_descending';

	// Department level orderby.
	const LEVDEPTORDERBY    = 'department_level_sort_each_faculty_&_staff_by';
	const LEVDEPTORDER      = 'department_level_designates_the_ascending_or_descending_order';
	const LEVDEPTAOSORDERBY = 'areas_dept_level_sort_each_faculty_&_staff_by';
	const LEVDEPTAOSORDER   = 'areas_dept_level_designates_the_ascending_or_descending_order';

	/**
	 * Sets up WordPress hooks/actions.
	 */
	public function __construct() {

		// Adding the function to the WordPress init.
		add_action( 'init', array( &$this, 'register_our_post_type' ) );

		// Register ACF fields.
		add_action( 'acf/init', array( $this, 'post_type_fields' ) );

		// Register our own sidebar.
		add_action( 'widgets_init', array( $this, 'register_sidebar' ) );

		// Filter post dropdown.
		add_action( 'restrict_manage_posts', array( $this, 'filter_posts_dropdown' ) );

		// WP_Query for taxonomy filter in admin.
		add_filter( 'parse_query', array( $this, 'query_taxonomy_filter' ) );

		// Add column to admin table.
		add_action( 'manage_faculty_staff_posts_columns', array( $this, 'add_department_column' ), 10, 2 );

		// Show content for department column.
		add_action( 'manage_faculty_staff_posts_custom_column', array( $this, 'show_department_column' ), 10, 2 );

		// Settings page.
		add_action( 'acf/init', array( $this, 'options_init' ) );

		// ACF fields for settings.
		add_action( 'acf/init', array( $this, 'set_settings_fields' ) );

	}

	/**
	 * Registers our post type and taxonomies
	 */
	public function register_our_post_type() {

		/* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		register_post_type(
			Tcu_Faculty_Staff_Main::POSTTYPE,
			// Let's now add all the options for this post type.
			array(
				'labels'          => array(
					'name'               => __( 'Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'singular_name'      => __( 'Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'all_items'          => __( 'All Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'add_new'            => __( 'Add New', 'tcu_faculty_staff_posttype' ),
					'add_new_item'       => __( 'Add New Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'edit'               => __( 'Edit', 'tcu_faculty_staff_posttype' ),
					'edit_item'          => __( 'Edit Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'new_item'           => __( 'New Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'view_item'          => __( 'View Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'search_items'       => __( 'Search Faculty & Staff', 'tcu_faculty_staff_posttype' ),
					'not_found'          => __( 'Nothing found in the Database.', 'tcu_faculty_staff_posttype' ),
					'not_found_in_trash' => __( 'Nothing found in Trash', 'tcu_faculty_staff_posttype' ),
				),
				'description'     => __( 'This is the staff post type', 'tcu_faculty_staff_posttype' ),
				'public'          => true,
				'has_archive'     => true,
				'supports'        => array( 'title', 'page-attributes', 'author' ),
				'menu_position'   => 20,
				'menu_icon'       => 'dashicons-id-alt',
				'capability_type' => array( 'tcu_employee', 'tcu_faculty_staff' ),
				'map_meta_cap'    => true,
			)
		); /* end of register post type */

		// Taxonomies.
		register_taxonomy_for_object_type( Tcu_Faculty_Staff_Main::DEPARTMENT, Tcu_Faculty_Staff_Main::POSTTYPE );
		register_taxonomy_for_object_type( Tcu_Faculty_Staff_Main::AREASOFSTUDY, Tcu_Faculty_Staff_Main::POSTTYPE );
		register_taxonomy_for_object_type( Tcu_Faculty_Staff_Main::COLLEGEOFFICE, Tcu_Faculty_Staff_Main::POSTTYPE );

		register_taxonomy(
			Tcu_Faculty_Staff_Main::DEPARTMENT, Tcu_Faculty_Staff_Main::POSTTYPE,
			array(
				'labels'       => array(
					'name'              => __( 'Departments', 'tcu_faculty_staff_posttype' ),
					'singular_name'     => __( 'Department', 'tcu_faculty_staff_posttype' ),
					'search_items'      => __( 'Search Departments', 'tcu_faculty_staff_posttype' ),
					'all_items'         => __( 'All Departments', 'tcu_faculty_staff_posttype' ),
					'parent_item'       => __( 'Parent Department', 'tcu_faculty_staff_posttype' ),
					'parent_item_colon' => __( 'Parent Department:', 'tcu_faculty_staff_posttype' ),
					'edit_item'         => __( 'Edit Department', 'tcu_faculty_staff_posttype' ),
					'update_item'       => __( 'Update Department', 'tcu_faculty_staff_posttype' ),
					'add_new_item'      => __( 'Add New Department', 'tcu_faculty_staff_posttype' ),
					'new_item_name'     => __( 'New Department Name', 'tcu_faculty_staff_posttype' ),
				),
				'hierarchical' => true,
				'show_ui'      => true,
				'query_var'    => true,
				'rewrite'      => array( 'hierarchical' => true ),
				'capabilities' => array(
					'manage_terms' => 'publish_tcu_faculty_staff',
					'edit_terms'   => 'publish_tcu_faculty_staff',
					'delete_terms' => 'publish_tcu_faculty_staff',
					'assign_terms' => 'edit_tcu_faculty_staff',
				),
			)
		);

		register_taxonomy(
			Tcu_Faculty_Staff_Main::AREASOFSTUDY, Tcu_Faculty_Staff_Main::POSTTYPE,
			array(
				'labels'       => array(
					'name'              => __( 'Areas of Study', 'tcu_faculty_staff_posttype' ),
					'singular_name'     => __( 'Areas of Study', 'tcu_faculty_staff_posttype' ),
					'search_items'      => __( 'Search Areas of Study', 'tcu_faculty_staff_posttype' ),
					'all_items'         => __( 'All Areas of Study', 'tcu_faculty_staff_posttype' ),
					'parent_item'       => __( 'Parent Areas of Study', 'tcu_faculty_staff_posttype' ),
					'parent_item_colon' => __( 'Parent Areas of Study:', 'tcu_faculty_staff_posttype' ),
					'edit_item'         => __( 'Edit Areas of Study', 'tcu_faculty_staff_posttype' ),
					'update_item'       => __( 'Update Areas of Study', 'tcu_faculty_staff_posttype' ),
					'add_new_item'      => __( 'Add New Areas of Study', 'tcu_faculty_staff_posttype' ),
					'new_item_name'     => __( 'New Areas of Study Name', 'tcu_faculty_staff_posttype' ),
				),
				'hierarchical' => true,
				'show_ui'      => true,
				'query_var'    => true,
				'rewrite'      => true,
				'capabilities' => array(
					'manage_terms' => 'publish_tcu_faculty_staff',
					'edit_terms'   => 'publish_tcu_faculty_staff',
					'delete_terms' => 'publish_tcu_faculty_staff',
					'assign_terms' => 'edit_tcu_faculty_staff',
				),
			)
		);

		register_taxonomy(
			Tcu_Faculty_Staff_Main::COLLEGEOFFICE, Tcu_Faculty_Staff_Main::POSTTYPE,
			array(
				'labels'       => array(
					'name'              => __( 'College Office Types', 'tcu_faculty_staff_posttype' ),
					'singular_name'     => __( 'College Office Types', 'tcu_faculty_staff_posttype' ),
					'search_items'      => __( 'Search College Office Types', 'tcu_faculty_staff_posttype' ),
					'all_items'         => __( 'All College Office Types', 'tcu_faculty_staff_posttype' ),
					'parent_item'       => __( 'Parent College Office Types', 'tcu_faculty_staff_posttype' ),
					'parent_item_colon' => __( 'Parent College Office Types:', 'tcu_faculty_staff_posttype' ),
					'edit_item'         => __( 'Edit College Office Types', 'tcu_faculty_staff_posttype' ),
					'update_item'       => __( 'Update College Office Types', 'tcu_faculty_staff_posttype' ),
					'add_new_item'      => __( 'Add New College Office Types', 'tcu_faculty_staff_posttype' ),
					'new_item_name'     => __( 'New College Office Types', 'tcu_faculty_staff_posttype' ),
				),
				'hierarchical' => true,
				'show_ui'      => true,
				'query_var'    => true,
				'rewrite'      => false,
				'capabilities' => array(
					'manage_terms' => 'publish_tcu_faculty_staff',
					'edit_terms'   => 'publish_tcu_faculty_staff',
					'delete_terms' => 'publish_tcu_faculty_staff',
					'assign_terms' => 'edit_tcu_faculty_staff',
				),
			)
		);
	}

	/**
	 * Register a new sidebar for our post type
	 */
	public function register_sidebar() {
		register_sidebar(
			array(
				'name'          => __( 'Faculty & Staff Sidebar', 'tcu_faculty_staff_posttype' ),
				'id'            => 'tcu-fs-sidebar',
				'description'   => __( 'Widgets in this area will be shown on all single faculty & staff pages.', 'tcu_faculty_staff_posttype' ),
				'before_widget' => '<div id="%1$s" class="tcu-sidebar__widget %2$s cf">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="tcu-sidebar__widget-title">',
				'after_title'   => '</h4>',
			)
		);
	}

	/**
	 * ACF fields
	 * This is generated by the ACF plugin
	 */
	public function post_type_fields() {
		acf_add_local_field_group(
			array(
				'key'                   => 'group_58407bd1a67c7',
				'title'                 => 'Faculty & Staff Information',
				'fields'                => array(
					array(
						'key'               => 'field_595e489db1644',
						'label'             => 'First Name',
						'name'              => 'tcu_first_name',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
						'maxlength'         => '',
					),
					array(
						'key'               => 'field_58407bf273c27',
						'label'             => 'Last Name',
						'name'              => 'tcu_last_name',
						'type'              => 'text',
						'instructions'      => 'Enter you last name for ordering purposes.',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'maxlength'         => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
					),
					array(
						'key'               => 'field_58407f24f511f',
						'label'             => 'Thumbnail',
						'name'              => 'tcu_thumbnail',
						'type'              => 'image',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'return_format'     => 'array',
						'preview_size'      => 'staff_thumbnail',
						'library'           => 'all',
						'min_width'         => '',
						'min_height'        => '',
						'min_size'          => '',
						'max_width'         => '',
						'max_height'        => '',
						'max_size'          => '',
						'mime_types'        => 'jpeg, jpg',
					),
					array(
						'key'               => 'field_58407c5a73c28',
						'label'             => 'Title',
						'name'              => 'tcu_title',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'maxlength'         => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
					),
					array(
						'key'               => 'field_58407cf773c29',
						'label'             => 'Office Location',
						'name'              => 'tcu_office_location',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'maxlength'         => '',
						'placeholder'       => '',
						'prepend'           => '',
						'append'            => '',
					),
					array(
						'key'               => 'field_58407d5b73c2a',
						'label'             => 'Email',
						'name'              => 'tcu_email',
						'type'              => 'email',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'placeholder'       => 'name@tcu.edu',
						'prepend'           => '',
						'append'            => '',
					),
					array(
						'key'               => 'field_58407de4f511d',
						'label'             => 'Phone Number',
						'name'              => 'tcu_phone_number',
						'type'              => 'text',
						'instructions'      => '',
						'required'          => 1,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'maxlength'         => '',
						'placeholder'       => '817-257-xxxx',
						'prepend'           => '',
						'append'            => '',
					),
					array(
						'key'               => 'field_58407eccf511e',
						'label'             => 'CV',
						'name'              => 'tcu_cv',
						'type'              => 'file',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'return_format'     => 'url',
						'library'           => 'uploadedTo',
						'min_size'          => '',
						'max_size'          => '',
						'mime_types'        => '',
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'faculty_staff',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'left',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			)
		);

		acf_add_local_field_group(
			array(
				'key'                   => 'group_5840811a3c96d',
				'title'                 => 'Faculty & Staff',
				'fields'                => array(
					array(
						'key'               => 'field_584081374e6b7',
						'label'             => 'Education',
						'name'              => 'tcu_education',
						'type'              => 'textarea',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'new_lines'         => 'wpautop',
						'maxlength'         => '',
						'placeholder'       => '',
						'rows'              => 5,
					),
					array(
						'key'               => 'field_584081f74e6b8',
						'label'             => 'Courses Taught',
						'name'              => 'tcu_courses_taught',
						'type'              => 'textarea',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'new_lines'         => 'wpautop',
						'maxlength'         => '',
						'placeholder'       => '',
						'rows'              => 5,
					),
					array(
						'key'               => 'field_5840821e4e6b9',
						'label'             => 'Areas of Focus',
						'name'              => 'tcu_areas_of_focus',
						'type'              => 'textarea',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'default_value'     => '',
						'new_lines'         => 'wpautop',
						'maxlength'         => '',
						'placeholder'       => '',
						'rows'              => 5,
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'faculty_staff',
						),
					),
				),
				'menu_order'            => 1,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'left',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			)
		);

		acf_add_local_field_group(
			array(
				'key'                   => 'group_584083141f709',
				'title'                 => 'Faculty & Staff Accordion',
				'fields'                => array(
					array(
						'key'               => 'field_584086a484de0',
						'label'             => 'Faculty & Staff',
						'name'              => 'faculty_and_staff_accordion',
						'type'              => 'repeater',
						'instructions'      => 'Click on the Add Accordion button to get started.',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'min'               => 0,
						'max'               => 0,
						'layout'            => 'block',
						'button_label'      => '+ Add Accordion',
						'collapsed'         => 'field_584086e484de1',
						'sub_fields'        => array(
							array(
								'key'               => 'field_584086e484de1',
								'label'             => 'Title',
								'name'              => 'staff_title',
								'type'              => 'text',
								'instructions'      => '',
								'required'          => 1,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'default_value'     => '',
								'maxlength'         => '',
								'placeholder'       => '',
								'prepend'           => '',
								'append'            => '',
							),
							array(
								'key'               => 'field_5840871a84de2',
								'label'             => 'Content',
								'name'              => 'staff_content',
								'type'              => 'wysiwyg',
								'instructions'      => '',
								'required'          => 0,
								'conditional_logic' => 0,
								'wrapper'           => array(
									'width' => '',
									'class' => '',
									'id'    => '',
								),
								'tabs'              => 'visual',
								'toolbar'           => 'basic',
								'media_upload'      => 1,
								'default_value'     => '',
								'delay'             => 0,
							),
						),
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => Tcu_Faculty_Staff_Main::POSTTYPE,
						),
					),
				),
				'menu_order'            => 2,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			)
		);

	}

	/**
	 * Create the dropdown so we can filter
	 * the admin by taxonomy
	 */
	public function filter_posts_dropdown() {
		global $typenow;

		if ( Tcu_Faculty_Staff_Main::POSTTYPE === $typenow ) {

			$args       = array(
				'public'   => true,
				'_builtin' => false,
			);
			$post_types = get_post_types( $args );

			if ( in_array( $typenow, $post_types, true ) ) {

				$taxonomy = get_object_taxonomies( $typenow );

				foreach ( $taxonomy as $tax_slug ) {

					$tax_obj = get_taxonomy( $tax_slug );
					$current = ( isset( $_GET[ $tax_obj->query_var ] ) ? $_GET[ $tax_obj->query_var ] : '' );

					wp_dropdown_categories(
						array(
							'show_option_all' => __( 'Show All ' . $tax_obj->label, 'tcu_faculty_staff_posttype' ),
							'taxonomy'        => $tax_slug,
							'name'            => $tax_obj->name,
							'orderby'         => 'term_order',
							'selected'        => $current,
							'hierarchical'    => $tax_obj->hierarchical,
							'show_count'      => false,
							'hide_empty'      => true,
						)
					);
				}
			}
		}
	}

	/**
	 * The WP_Query to filter the admin
	 * by taxonomy
	 *
	 * @param Object $query Query object.
	 */
	public function query_taxonomy_filter( $query ) {
		global $pagenow, $typenow;

		if ( Tcu_Faculty_Staff_Main::POSTTYPE === $typenow ) {

			if ( 'edit.php' === $pagenow ) {

				$taxonomy = get_object_taxonomies( $typenow );

				foreach ( $taxonomy as $tax_slug ) {

					$var = &$query->query_vars[ $tax_slug ];

					if ( isset( $var ) ) {

						$term = get_term_by( 'id', $var, $tax_slug );

						if ( $term ) {
							$var = $term->slug;
						}
					}
				}
			}
		}
	}

	/**
	 * Replace table columns in edit.php
	 *
	 * @param  array $columns Array of Columns.
	 * @return array Array of columns to be replaced.
	 */
	public function add_department_column( $columns ) {
		return array(
			'cb'             => '<input type="checkbox" />',
			'title'          => __( 'Title', 'tcu_faculty_staff_posttype' ),
			'department'     => __( 'Department', 'tcu_faculty_staff_posttype' ),
			'college_office' => __( 'College Office Types', 'tcu_faculty_staff_posttype' ),
			'areas_of_study' => __( 'Areas of Study', 'tcu_faculty_staff_posttype' ),
		);
	}

	/**
	 * Add content to table columns in edit.php
	 *
	 * @param string $column The name of the column to display.
	 * @param int    $post_id The ID of the current post.
	 */
	public function show_department_column( $column, $post_id ) {
		global $typenow;

		if ( Tcu_Faculty_Staff_Main::POSTTYPE === $typenow ) {

			$department     = Tcu_Faculty_Staff_Main::DEPARTMENT;
			$areas_of_study = Tcu_Faculty_Staff_Main::AREASOFSTUDY;
			$college_types  = Tcu_Faculty_Staff_Main::COLLEGEOFFICE;

			switch ( $column ) {

				case $department:
					$departments = wp_get_post_terms(
						get_the_ID(), $department, array(
							'fields' => 'names',
							'parent' => 0,
						)
					);

					if ( is_array( $departments ) && ! empty( $departments ) ) {

						$num                 = key( $departments );
						$departments[ $num ] = $departments[ $num ];

						// Return only parent.
						echo $departments[0];
					}

					break;

				case $areas_of_study:
					$areas_of_study = get_the_terms( $post_id, $areas_of_study );

					if ( is_array( $areas_of_study ) ) {

						foreach ( $areas_of_study as $key => $area ) {
							$areas_of_study[ $key ] = $area->name;
						}

						echo implode( ' | ', $areas_of_study );
					}

					break;

				case $college_types:
					$college_types = get_the_terms( $post_id, $college_types );

					if ( is_array( $college_types ) ) {

						foreach ( $college_types as $key => $type ) {
							$college_types[ $key ] = $type->name;
						}

						echo implode( ' | ', $college_types );
					}

					break;
			}
		}
	}

	/**
	 * Options page
	 * ACF set up
	 */
	public function options_init() {
		if ( function_exists( 'acf_add_options_sub_page' ) ) {
			$option_page = acf_add_options_sub_page(
				array(
					'menu_title'  => __( 'Settings', 'tcu_faculty_staff_posttype' ),
					'parent_slug' => 'edit.php?post_type=faculty_staff',
					'capability'  => 'publish_tcu_faculty_staff',
				)
			);
		}
	}

	/**
	 * ACF Fields
	 * This is automatically generated by the ACF plugin
	 */
	public function set_settings_fields() {
		acf_add_local_field_group(
			array(
				'key'                   => 'group_58ffa87572de7',
				'title'                 => 'College Level Tab Views',
				'fields'                => array(
					array(
						'key'               => 'field_595e73d5a7d8d',
						'label'             => 'IMPORTANT',
						'name'              => '',
						'type'              => 'message',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'message'           => 'The tabs below control the view of the "Department" and "Areas Of Study" tabs.',
						'new_lines'         => 'wpautop',
						'esc_html'          => 0,
					),
					array(
						'key'               => 'field_58ffa87c29ae7',
						'label'             => 'Department Tab',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_58ffa8e929ae8',
						'label'             => 'Display Department Tab?',
						'name'              => 'display_department_tab',
						'type'              => 'true_false',
						'instructions'      => 'Please uncheck if you would like to remove the "Department" tab from the Faculty/Staff page.',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'message'           => '',
						'default_value'     => 1,
						'ui'                => 0,
						'ui_on_text'        => '',
						'ui_off_text'       => '',
					),
					array(
						'key'               => 'field_58ffa94cdfdeb',
						'label'             => 'Areas of Study Tab',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_58ffa95ddfdec',
						'label'             => 'Display Areas of Study Tab?',
						'name'              => 'display_areas_of_study_tab',
						'type'              => 'true_false',
						'instructions'      => 'Please uncheck if you would like to remove the "Areas of Study" tab from the Faculty/Staff page.',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'message'           => '',
						'default_value'     => 1,
						'ui'                => 0,
						'ui_on_text'        => '',
						'ui_off_text'       => '',
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'options_page',
							'operator' => '==',
							'value'    => 'acf-options-settings',
						),
					),
				),
				'menu_order'            => 0,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			)
		);

		acf_add_local_field_group(
			array(
				'key'                   => 'group_595e654405a2f',
				'title'                 => 'College Level Order',
				'fields'                => array(
					array(
						'key'               => 'field_595e6b9bb78fb',
						'label'             => 'Instructions',
						'name'              => '',
						'type'              => 'message',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'message'           => 'Each tab at the college level is automatically ordered by last name in alphabetical order. You can modify the order of each individual tab below.',
						'new_lines'         => 'wpautop',
						'esc_html'          => 0,
					),
					array(
						'key'               => 'field_595e6586bd80c',
						'label'             => 'Areas of Study',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_595e65e696ff9',
						'label'             => 'Sort each faculty & staff by:',
						'name'              => 'areas_sort_each_faculty_&_staff_by',
						'type'              => 'radio',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'alpha_name' => 'Last name, First Name',
							'menu_order' => 'Page Order',
							'date'       => 'Order by date',
							'modified'   => 'Order by last date modified',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'alpha_name',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e677baeaef',
						'label'             => 'Designates the ascending or descending order',
						'name'              => 'areas_ascending_descending',
						'type'              => 'radio',
						'instructions'      => '\'ASC\' - ascending order from lowest to highest values (1, 2, 3; a, b, c).
        \'DESC\' - descending order from highest to lowest values (3, 2, 1; c, b, a).',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'ASC'  => 'ASC',
							'DESC' => 'DESC',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'ASC',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e68991bd17',
						'label'             => 'Department',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_595e68b870a65',
						'label'             => 'Sort each faculty & staff by:',
						'name'              => 'department_sort_each_faculty_&_staff_by',
						'type'              => 'radio',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'alpha_name' => 'Last name, First Name',
							'menu_order' => 'Page Order',
							'date'       => 'Order by date',
							'modified'   => 'Order by last date modified',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'alpha_name',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e68e6a5715',
						'label'             => 'Designates the ascending or descending order',
						'name'              => 'department_areas_ascending_descending',
						'type'              => 'radio',
						'instructions'      => '\'ASC\' - ascending order from lowest to highest values (1, 2, 3; a, b, c).
        \'DESC\' - descending order from highest to lowest values (3, 2, 1; c, b, a).',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'ASC'  => 'ASC',
							'DESC' => 'DESC',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'ASC',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e6917ce1cd',
						'label'             => 'College Office Types',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_595e6cb340424',
						'label'             => 'IMPORTANT',
						'name'              => '',
						'type'              => 'message',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'message'           => 'The College Office Types include the order of ALL the terms within that taxonomy. For example: College Office Types taxonomy include the terms chairs, deans\' office, directors, staff. The tab below will modify the order for ALL those tabs.

        - College Types
        --- Chairs
        --- Dean\'s Office
        --- Directors
        --- Staff',
						'new_lines'         => 'wpautop',
						'esc_html'          => 0,
					),
					array(
						'key'               => 'field_595e69453d571',
						'label'             => 'Sort each faculty & staff by:',
						'name'              => 'college_types_sort_each_faculty_&_staff_by',
						'type'              => 'radio',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'alpha_name' => 'Last name, First Name',
							'menu_order' => 'Page Order',
							'date'       => 'Order by date',
							'modified'   => 'Order by last date modified',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'alpha_name',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e694b3d572',
						'label'             => 'Designates the ascending or descending order',
						'name'              => 'college_types_ascending_descending',
						'type'              => 'radio',
						'instructions'      => '\'ASC\' - ascending order from lowest to highest values (1, 2, 3; a, b, c).
        \'DESC\' - descending order from highest to lowest values (3, 2, 1; c, b, a).',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'ASC'  => 'ASC',
							'DESC' => 'DESC',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'ASC',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'options_page',
							'operator' => '==',
							'value'    => 'acf-options-settings',
						),
					),
				),
				'menu_order'            => 1,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			)
		);

		acf_add_local_field_group(
			array(
				'key'                   => 'group_595e6eed6198e',
				'title'                 => 'Department Level Order',
				'fields'                => array(
					array(
						'key'               => 'field_595e70848b048',
						'label'             => 'IMPORTANT',
						'name'              => '',
						'type'              => 'message',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'message'           => 'Each department\'s faculty & staff page is ordered by last name in alphabetical order. You can modify the order of each page below.

        The tabs below will modify ALL individual department pages.
        Example: <a href="https://addran.tcu.edu/department/criminal-justice/">https://addran.tcu.edu/department/criminal-justice/</a>

        The tabs below will also modify ALL individual areas of study pages.
        Example: <a href="https://addran.tcu.edu/areas_of_study/asian-studies/">https://addran.tcu.edu/areas_of_study/asian-studies/</a>',
						'new_lines'         => 'wpautop',
						'esc_html'          => 0,
					),
					array(
						'key'               => 'field_595e6f08859a7',
						'label'             => 'Department',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_595e6f3d4cac0',
						'label'             => 'Sort each faculty & staff by',
						'name'              => 'department_level_sort_each_faculty_&_staff_by',
						'type'              => 'radio',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'alpha_name' => 'Last name, First Name',
							'menu_order' => 'Page Order',
							'date'       => 'Order by date',
							'modified'   => 'Order by last date modified',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'alpha_name',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e6f964cac1',
						'label'             => 'Designates the ascending or descending order',
						'name'              => 'department_level_designates_the_ascending_or_descending_order',
						'type'              => 'radio',
						'instructions'      => '\'ASC\' - ascending order from lowest to highest values (1, 2, 3; a, b, c).
        \'DESC\' - descending order from highest to lowest values (3, 2, 1; c, b, a).',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'ASC'  => 'ASC',
							'DESC' => 'DESC',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'ASC',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e6fbf4cac2',
						'label'             => 'Areas of Study',
						'name'              => '',
						'type'              => 'tab',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'placement'         => 'top',
						'endpoint'          => 0,
					),
					array(
						'key'               => 'field_595e6fce4cac3',
						'label'             => 'Sort each faculty & staff by:',
						'name'              => 'areas_dept_level_sort_each_faculty_&_staff_by',
						'type'              => 'radio',
						'instructions'      => '',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'alpha_name' => 'Last name, First Name',
							'menu_order' => 'Page Order',
							'date'       => 'Order by date',
							'modified'   => 'Order by last date modified',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'alpha_name',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
					array(
						'key'               => 'field_595e70074cac4',
						'label'             => 'Designates the ascending or descending order',
						'name'              => 'areas_dept_level_designates_the_ascending_or_descending_order',
						'type'              => 'radio',
						'instructions'      => '\'ASC\' - ascending order from lowest to highest values (1, 2, 3; a, b, c).
        \'DESC\' - descending order from highest to lowest values (3, 2, 1; c, b, a).',
						'required'          => 0,
						'conditional_logic' => 0,
						'wrapper'           => array(
							'width' => '',
							'class' => '',
							'id'    => '',
						),
						'choices'           => array(
							'ASC'  => 'ASC',
							'DESC' => 'DESC',
						),
						'allow_null'        => 0,
						'other_choice'      => 0,
						'save_other_choice' => 0,
						'default_value'     => 'ASC',
						'layout'            => 'horizontal',
						'return_format'     => 'value',
					),
				),
				'location'              => array(
					array(
						array(
							'param'    => 'options_page',
							'operator' => '==',
							'value'    => 'acf-options-settings',
						),
					),
				),
				'menu_order'            => 2,
				'position'              => 'normal',
				'style'                 => 'default',
				'label_placement'       => 'top',
				'instruction_placement' => 'label',
				'hide_on_screen'        => '',
				'active'                => 1,
				'description'           => '',
			)
		);
	}
}
