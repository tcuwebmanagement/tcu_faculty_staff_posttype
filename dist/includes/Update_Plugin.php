<?php
/**
 * Update plugin
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 *
 * @deprecated See file class-tcu-faculty-staff-update.php
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Class to connect to our API and update the plugin
 *
 * @deprecated Since 3.0.0 No longer used by internal code and not recommended.
 */
class Tcu_Faculty_Staff_Update {

	const URL  = 'https://webmanage.tcu.edu/api/';
	const SLUG = 'tcu_faculty_staff_posttype';

	/**
	 * Constructor
	 */
	public function __construct() {
		// Take over the update check.
		add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'check_for_update' ) );

		// Take over the Plugin info screen.
		add_filter( 'plugins_api', array( $this, 'api_call' ), 10, 3 );
	}

	/**
	 * Check for updates
	 *
	 * @param mixed $transient Value of site transient. Expected to not be SQL-escaped.
	 * @return mixed $transient
	 */
	public function check_for_update( $transient ) {

		// Comment out these two lines during testing.
		if ( empty( $transient->checked ) ) {
			return $transient;
		}

		$args = array(
			'slug'    => self::SLUG,
			'version' => $transient->checked[ self::SLUG . '/' . self::SLUG . '.php' ],
		);

		$request_string = array(
			'body'       => array(
				'action'  => 'basic_check',
				'request' => serialize( $args ),
				'api-key' => md5( get_bloginfo( 'url' ) ),
			),
			'user-agent' => 'WordPress/' . Tcu_Faculty_Staff_Main::VERSION . '; ' . get_bloginfo( 'url' ),
		);

		// Start checking for an update.
		$raw_response = wp_remote_post( self::URL, $request_string );

		if ( ! is_wp_error( $raw_response ) && ( 200 === $raw_response['response']['code'] ) ) {
			$response = unserialize( $raw_response['body'] );
		}

		if ( is_object( $response ) && ! empty( $response ) ) {
			// Feed the update data into WP updater.
			$transient->response[ self::SLUG . '/' . self::SLUG . '.php' ] = $response;
		}

		return $transient;
	}


	/**
	 * Display update information
	 *
	 * @param array  $results The result object or array. Default false.
	 * @param string $action  API action to perform: 'plugin_information'.
	 * @param mixed  $args    Array/object of arguments to serialize for the Plugin Info API.
	 */
	public function api_call( $results, $action, $args ) {

		if ( ! isset( $args->slug ) || ( self::SLUG !== $args->slug ) ) {
			return $results;
		}

		// Get the current version.
		$plugin_info     = get_site_transient( 'update_plugins' );
		$current_version = $plugin_info->checked[ self::SLUG . '/' . self::SLUG . '.php' ];
		$args->version   = $current_version;

		$request_string = array(
			'body'       => array(
				'action'  => $action,
				'request' => serialize( $args ),
				'api-key' => md5( get_bloginfo( 'url' ) ),
			),
			'user-agent' => 'WordPress/' . Tcu_Faculty_Staff_Main::VERSION . '; ' . get_bloginfo( 'url' ),
		);

		$request = wp_remote_post( self::URL, $request_string );

		if ( is_wp_error( $request ) ) {
			$results = new WP_Error( 'plugins_api_failed', __( 'An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>', 'tcu_faculty_staff_posttype' ), $request->get_error_message() );
		} else {
			$results = unserialize( $request['body'] );

			if ( false === $results ) {
				$results = new WP_Error( 'plugins_api_failed', __( 'An unknown error occurred', 'tcu_faculty_staff_posttype' ), $request['body'] );
			}
		}

		return $results;
	}
}


