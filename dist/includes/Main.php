<?php
/**
 * Main class
 *
 * @since TCU Faculty Staff Post Type 3.0.0
 * @package tcu_faculty_staff_posttype
 *
 * @deprecated See file name class-tcu-faculty-staff-main.php
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Register our plugin and instatiate our classes
 *
 * @deprecated Since 3.0.0 No longer used by internal code and not recommended.
 */
class Tcu_Faculty_Staff_Main {

	const VERSION       = '3.0.0';
	const POSTTYPE      = 'faculty_staff';
	const DEPARTMENT    = 'department';
	const AREASOFSTUDY  = 'areas_of_study';
	const COLLEGEOFFICE = 'college_office';
	const TRANSIENTSLUG = '_fs_transient_';

	/**
	 * Static instance of self
	 *
	 * @var object The instance of this class.
	 */
	protected static $self;

	/**
	 * Instance of Tcu_Faculty_Staff_Display
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Display class.
	 */
	protected $display;

	/**
	 * Instance of Tcu_Faculty_Staff_Capabilities
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Capabilities class.
	 */
	protected $caps;

	/**
	 * Instance of Tcu_Faculty_Staff_Admin
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Admin class.
	 */
	protected $admin;

	/**
	 * Instance of Tcu_Faculty_Staff_Update
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Update class.
	 */
	protected $update;

	/**
	 * Get (and instantiate, if necessary) the instance of the class
	 *
	 * @return self
	 */
	public static function instance() {
		if ( ! self::$self ) {
			self::$self = new self();
		}
		return self::$self;
	}

	/**
	 * Initializes plugin variables and sets up WordPress hooks/actions.
	 */
	protected function __construct() {
		// Load our include files.
		$this->load_includes();

		// Initiate Plugin (activation hook).
		register_activation_hook( __FILE__, array( $this, 'initiate_plugin' ) );

		// Deactivate plugin hook.
		register_deactivation_hook( __FILE__, array( $this, 'deactivate_plugin' ) );

		// Register our post type.
		$this->instantiate_admin_class();

		// Instance of capabilities class.
		$this->instantiate_caps_class();

		// Instance of our display class.
		$this->instantiate_display_class();

		// Update our plugin.
		$this->instantiate_update_class();

	}

	/**
	 * Instantiate class
	 */
	public function instantiate_display_class() {
		if ( ! $this->display ) {
			$this->display = new Tcu_Faculty_Staff_Display();
		}
		return $this->display;
	}

	/**
	 * Instantiate class
	 */
	public function instantiate_caps_class() {
		if ( ! $this->caps ) {
			$this->caps = new Tcu_Faculty_Staff_Capabilities();
		}
		return $this->caps;
	}

	/**
	 * Instantiate class
	 */
	public function instantiate_admin_class() {
		if ( ! $this->admin ) {
			$this->admin = new Tcu_Faculty_Staff_Admin();
		}
		return $this->admin;
	}

	/**
	 * Instantiate class
	 */
	public function instantiate_update_class() {
		if ( ! $this->update ) {
			$this->update = new Tcu_Faculty_Staff_Update();
		}
		return $this->update;
	}

	/**
	 * Register plugin hook
	 */
	public function initiate_plugin() {
		// Register post type.
		$this->admin->register_post_type();

		// Set initial user capabilities.
		$this->caps->set_initial_caps();

		// Remove older capabilites from older plugin versions.
		$this->caps->remove_old_caps();

		// Flush rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Deactivate plugin hook
	 */
	public function deactivate_plugin() {
		// Remove initial user capabilities.
		$this->caps->remove_all_caps();

		// Flush rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Load our dependencies
	 */
	protected function load_includes() {
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/Display_Post_Types.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/Faculty_Staff_Admin.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/Capabilities.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/class-tcu-faculty-staff-update.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/helpers.php';
	}
}
