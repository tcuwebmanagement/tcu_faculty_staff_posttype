<?php
/**
 * Main class
 *
 * @since TCU Faculty Staff Post Type 3.0.0
 * @package tcu_faculty_staff_posttype
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Register our plugin and instatiate our classes
 */
class Tcu_Faculty_Staff_Main {

	const VERSION       = '3.1.0';
	const POSTTYPE      = 'faculty_staff';
	const DEPARTMENT    = 'department';
	const AREASOFSTUDY  = 'areas_of_study';
	const COLLEGEOFFICE = 'college_office';
	const TRANSIENTSLUG = '_fs_transient_';

	/**
	 * Static instance of self
	 *
	 * @var object The instance of this class.
	 */
	protected static $self;

	/**
	 * Instance of Tcu_Faculty_Staff_Display
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Display class.
	 */
	protected $display;

	/**
	 * Instance of Tcu_Faculty_Staff_Capabilities
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Capabilities class.
	 */
	protected $caps;

	/**
	 * Instance of Tcu_Faculty_Staff_Admin
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Admin class.
	 */
	protected $admin;

	/**
	 * Instance of Tcu_Faculty_Staff_Update
	 *
	 * @var object The instance of Tcu_Faculty_Staff_Update class.
	 */
	protected $update;

	/**
	 * Get (and instantiate, if necessary) the instance of the class
	 *
	 * @return self
	 */
	public static function instance() {
		if ( ! self::$self ) {
			self::$self = new self();
		}
		return self::$self;
	}

	/**
	 * Initializes plugin variables and sets up WordPress hooks/actions.
	 */
	protected function __construct() {
		// Load our include files.
		$this->load_includes();

		// Register our post type.
		$this->instantiate_admin_class();

		// Instance of capabilities class.
		$this->instantiate_caps_class();

		// Instance of our display class.
		$this->instantiate_display_class();

		// Update our plugin.
		$this->instantiate_update_class();

		// Initiate Plugin (activation hook).
		register_activation_hook( __FILE__, array( $this, 'activate_plugin' ) );

		// Deactivate plugin hook.
		register_deactivation_hook( __FILE__, array( $this, 'deactivate_plugin' ) );

		// Display warning when ACF plugin is not intalled.
		add_action( 'admin_notices', array( $this, 'acf_plugin_notice' ) );

	}

	/**
	 * Instantiate class
	 */
	public function instantiate_admin_class() {
		if ( ! $this->admin ) {
			$this->admin = new Tcu_Faculty_Staff_Admin();
		}
		return $this->admin;
	}

	/**
	 * Instantiate class
	 */
	public function instantiate_display_class() {
		if ( ! $this->display ) {
			$this->display = new Tcu_Faculty_Staff_Display();
		}
		return $this->display;
	}

	/**
	 * Instantiate class
	 */
	public function instantiate_caps_class() {
		if ( ! $this->caps ) {
			$this->caps = new Tcu_Faculty_Staff_Capabilities();
		}
		return $this->caps;
	}


	/**
	 * Instantiate class
	 */
	public function instantiate_update_class() {
		if ( ! $this->update ) {
			$this->update = new Tcu_Faculty_Staff_Update();
		}
		return $this->update;
	}

	/**
	 * Register plugin hook
	 */
	public function activate_plugin() {
		// Register post type.
		$this->admin->register_our_post_type();

		// Set initial user capabilities.
		$this->caps->set_initial_caps();

		// Remove older capabilites from older plugin versions.
		$this->caps->remove_old_caps();

		// Flush rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Deactivate plugin hook
	 */
	public function deactivate_plugin() {
		// Remove initial user capabilities.
		$this->caps->remove_all_caps();

		// Flush rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Load our dependencies
	 */
	protected function load_includes() {
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/class-tcu-faculty-staff-admin.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/class-tcu-faculty-staff-display.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/class-tcu-faculty-staff-capabilities.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/class-tcu-faculty-staff-update.php';
		require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/helpers.php';
	}

	/**
	 * Check if the ACF plugin is intalled
	 *
	 * @return bool  True if the ACF plugin is installed
	 */
	public function is_acf_installed() {

		/**
		 * Checks to see if "is_plugin_active" function exists and if not load the php file that includes that function */
		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		// Checks to see if the acf pro plugin is activated.
		if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) || is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Admin notice to display when ACF is not installed
	 */
	public function acf_plugin_notice() {
		global $tcu_faculty_staff_posttype_acf_warning;

		$tcu_faculty_staff_posttype_acf_warning = esc_html__( 'The Faculty & Staff plugin needs the "Advanced Custom Fields PRO" plugin to run. Please download and activate it.', 'tcu_faculty_staff_posttype' );

		$allowed_tags = array(
			'div' => array(
				'class' => array(),
			),
		);

		if ( ! $this->is_acf_installed() ) {
			echo wp_kses( '<div class="notice notice-error notice-large"><div class="notice-title">' . $tcu_faculty_staff_posttype_acf_warning . '</div></div>', $allowed_tags );
		}
	}
}
