<?php
/**
 * Displays all Faculty & Staff
 * We seperate them by taxonomy (Areas of Study, Department, College Types)
 * We also include the WP Advance Search form to easily find a faculty or staff
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

// Let's collect our College Office Type terms.
$tcu_college_office = get_terms(
	array(
		'taxonomy'   => Tcu_Faculty_Staff_Main::COLLEGEOFFICE,
		'childless'  => true,
		'orderby'    => 'term_id',
		'order'      => 'ASC',
		'hide_empty' => true,
	)
);

/**
 * The option controls the display of Areas of Study and Department tabs
 * This option is located in the settings page
 */
$tcu_display_aos_tab  = get_field( Tcu_Faculty_Staff_Admin::AREASOFSTUDYTAB, 'option' );
$tcu_display_dept_tab = get_field( Tcu_Faculty_Staff_Admin::DEPARTMENTTAB, 'option' );

// Grab our header.
get_header();

// Display breadcrumbs from parent theme.
if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
} ?>

	<div class="tcu-layoutwrap--transparent">

		<div class="tcu-layout-constrain cf">

			<main class="unit size1of1 m-size1of1 main cf" id="main">

				<?php
				// The title.
				the_archive_title( '<h1>', '</h1>' );
				?>

				<!-- Begin our tabs -->
				<div class="tcu-responsive-tabs">

					<?php
					// Our UL for the tabs.
					require 'partials/tabs-faculty-staff-archive.php';

					/**
					 * Check settings page
					 * Should we display the Areas of Study tab?
					 */
					if ( $tcu_display_aos_tab ) :
					?>
						<div id="<?php echo esc_attr( Tcu_Faculty_Staff_Main::AREASOFSTUDY ); ?>">
							<?php include 'partials/loop-areas-of-study.php'; ?>
						</div>
					<?php
					endif;

					/**
					 * Check settings page
					 * Should we display the Department tab?
					 */
					if ( $tcu_display_dept_tab ) :
						?>
						<div id="<?php echo esc_attr( Tcu_Faculty_Staff_Main::DEPARTMENT ); ?>">
							<?php include 'partials/loop-department.php'; ?>
						</div>
					<?php
					endif;

					/**
					 * Begin our College Office Type's loop
					 */
					?>
					<?php foreach ( $tcu_college_office as $office ) : ?>
						<div id="<?php echo esc_attr( $office->slug ); ?>">
							<?php include 'partials/loop-college-office-types.php'; ?>
						</div><!-- End College level staff tabs -->
					<?php endforeach; ?>

				</div><!-- end of .tcu-responsive-tabs -->

			</main><!-- end of .unit -->

		</div><!-- end of .tcu-layout-constrain -->

	</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
