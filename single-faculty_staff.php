<?php
/**
 * Displays the single template for faculty & staff
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

get_header(); ?>

<?php
if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
}
?>

<div class="tcu-layoutwrap--transparent">

	<div class="tcu-layout-constrain cf">

		<main class="unit size2of3 m-size2of3 cf" id="main">

		<?php
		if ( have_posts() ) :
			while ( have_posts() ) :
				the_post();
		?>

			<article aria-labelledby="post-<?php the_ID(); ?>" <?php post_class( 'tcu-article cf' ); ?>>

				<header class="tcu-article__header cf">

					<?php
					/**
					 * ACF variables
					 */
					$tcu_thumbnail    = get_field( 'tcu_thumbnail' );
					$tcu_current_dept = wp_get_post_terms(
						get_the_ID(),
						'department', array(
							'fields' => 'names',
							'parent' => 0,
						)
					);
					$tcu_key          = key( $tcu_current_dept );
					?>

					<?php if ( $tcu_thumbnail ) : ?>
						<div class="size1of4 m-size3of8 tcu-pad-l0 unit">
							<img style="height: auto; margin: 0 0 4px 0; max-width: 100%;" src="<?php echo esc_url( $tcu_thumbnail['sizes']['staff_thumbnail'] ); ?>" alt="<?php echo esc_attr( $tcu_thumbnail['alt'] ); ?>">
						</div>
					<?php endif; ?>

					<div class="size3of4 m-size5of8 tcu-pad-l0 unit">

						<h1 id="post-<?php the_ID(); ?>" class="h2 tcu-mar-t0 tcu-mar-b0 tcu-line-height-1" itemprop="headline"><?php the_title(); ?></h1>
						<p>
							<?php echo esc_html( the_field( 'tcu_title' ) ); ?><br>
							<?php
							if ( $tcu_current_dept ) :
								echo esc_html( $tcu_current_dept[ $tcu_key ] );
							endif;
							?>
							<br>
							<?php
							if ( get_field( 'tcu_office_location' ) ) :
								echo esc_html( the_field( 'tcu_office_location' ) );
							endif;
							?>
						</p>

						<p>
							<a href="mailto:<?php echo esc_attr( the_field( 'tcu_email' ) ); ?>"><?php echo esc_html( the_field( 'tcu_email' ) ); ?></a> | <?php echo esc_html( the_field( 'tcu_phone_number' ) ); ?>
						</p>

						<?php if ( get_field( 'tcu_cv' ) ) : ?>
							<p>
								<a href="<?php echo esc_url( the_field( 'tcu_cv' ) ); ?>"><?php esc_html_e( 'Download CV', 'tcu_faculty_staff_posttype' ); ?></a>
							</p>
						<?php endif; ?>

					</div><!-- End of .unit -->

				</header>

				<div class="tcu-article__content cf">

					<?php if ( get_field( 'tcu_education' ) ) : ?>
						<h2 class="h3"><?php esc_html_e( 'Education', 'tcu_faculty_staff_posttype' ); ?></h2>
						<?php echo wp_kses_post( the_field( 'tcu_education' ) ); ?>
					<?php endif; ?>

					<?php if ( get_field( 'tcu_courses_taught' ) ) : ?>
						<h2 class="h3"><?php esc_html_e( 'Courses Taught', 'tcu_faculty_staff_posttype' ); ?></h2>
						<?php echo wp_kses_post( the_field( 'tcu_courses_taught' ) ); ?>
					<?php endif; ?>

					<?php if ( get_field( 'tcu_areas_of_focus' ) ) : ?>
						<h2 class="h3"><?php esc_html_e( 'Areas of Focus', 'tcu_faculty_staff_posttype' ); ?></h2>
						<?php echo wp_kses_post( the_field( 'tcu_areas_of_focus' ) ); ?>
					<?php endif; ?>

				</div><!-- end of .tcu-article__content -->

			<?php if ( have_rows( 'faculty_and_staff_accordion' ) ) : ?>

				<!-- Accordion -->
				<div class="tcu-article__content cf">

					<div class="tcu-accordion-container">

						<?php
						/**
						 * Start the ACF loop
						 */
						while ( have_rows( 'faculty_and_staff_accordion' ) ) :
							the_row();
						?>

						<button type="button" class="tcu-accordion-header h4"><?php the_sub_field( 'staff_title' ); ?></button>

						<div class="tcu-accordion-content cf">
							<?php the_sub_field( 'staff_content' ); ?>
						</div>

						<?php
						// End of the loop.
						endwhile;
						?>

					</div> <!-- end of .tcu-accordion-container -->

				</div><!-- end of .tcu-article__content -->

			<?php endif; ?>

			</article><!-- end of .tcu-article -->

			<?php
			// End of main while loop.
			endwhile;

				else :

					// If no content, include the "No posts found" template.
					include 'partials/content-none.php';

		// End of main loop.
		endif;
		?>

		</main><!-- end of .unit -->

		<?php require 'partials/faculty_staff-sidebar.php'; ?>

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
