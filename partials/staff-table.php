<?php
/**
 * The template part for displaying faculty & staff rows inside our table
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */

?>
<tr <?php post_class( 'tcu-article cf' ); ?> role="article">

	<?php
	/**
	 * Grab the department and array key
	 */
	$tcu_current_dept = wp_get_post_terms(
		get_the_ID(), 'department', array(
			'fields' => 'names',
			'parent' => 0,
		)
	);
	$tcu_key          = key( $tcu_current_dept );
	?>

	<td><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></td>
	<td><?php the_field( 'tcu_title' ); ?></td>
	<td><?php the_field( 'tcu_phone_number' ); ?></td>
	<td><a href="mailto:<?php the_field( 'tcu_email' ); ?>"><?php the_field( 'tcu_email' ); ?></a></td>
	<?php if ( $tcu_current_dept ) : ?>
	<td><?php echo esc_html( $tcu_current_dept[ $tcu_key ] ); ?></td>
	<?php endif; ?>

</tr><!-- end of table row -->
