<?php
/**
 * The template part for displaying the search form
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 1.0.0
 *
 * @deprecated Since 3.0.0 Do not use.
 */

?>
<div class="tcu-layoutwrap--grey tcu-pad-tb0 tcu-below64 cf">
	<?php
	if ( $tcu_search ) {
		$tcu_search->the_form();
	}
?>
</div>

<div class="tcu-below32">
	<div id="wpas-results"></div>
</div>
