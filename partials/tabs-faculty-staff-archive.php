<?php
/**
 * Tabs for archive-faculty_staff.php template
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty & Staff Post Type 3.1.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}
?>

<ul>
	<?php
	/**
	 * Check settings page
	 * Should we display the Areas of Study tab?
	 */
	if ( $tcu_display_aos_tab ) :
	?>
		<li>
			<a aria-label="<?php echo esc_html_e( 'View Faculty &amp; Staff filtered by Areas of Study', 'tcu_faculty_staff_posttype' ); ?>" href="#<?php echo esc_attr( Tcu_Faculty_Staff_Main::AREASOFSTUDY ); ?>">
				<?php echo esc_html_e( 'Areas of Study', 'tcu_faculty_staff_posttype' ); ?>
			</a>
		</li>
	<?php
	endif;

	/**
	 * Check settings page
	 * Should we display the Department tab?
	 */
	if ( $tcu_display_dept_tab ) :
	?>
		<li>
			<a aria-label="<?php echo esc_html_e( 'View Faculty &amp; Staff filtered by Department', 'tcu_faculty_staff_posttype' ); ?>" href="#<?php echo esc_attr( Tcu_Faculty_Staff_Main::DEPARTMENT ); ?>">
				<?php echo esc_html_e( 'Department', 'tcu_faculty_staff_posttype' ); ?>
			</a>
		</li>
	<?php endif; ?>

	<!-- Start College level staff -->
	<?php foreach ( $tcu_college_office as $office ) : ?>
		<li>
			<a aria-label="<?php echo esc_html__( 'View Faculty &amp; Staff filtered by ', 'tcu_faculty_staff_posttype' ) . esc_html( $office->name ); ?>" href="#<?php echo esc_attr( $office->slug ); ?>"><?php echo esc_html( $office->name ); ?></a>
		</li>
	<?php endforeach; ?>
</ul>
