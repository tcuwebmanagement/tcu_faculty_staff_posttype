<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 1.0.0
 */

?>

<article id="post-not-found" class="tcu-article hentry cf" role="article">
	<header class="tcu-article__header">
		<h1><?php esc_html_e( 'Oops, Nothing was found!', 'tcu_faculty_staff_posttype' ); ?></h1>
	</header>
	<section class="tcu-article__content" role="region">
		<p><?php esc_html_e( 'Uh Oh. Something is missing. Try double checking things.', 'tcu_faculty_staff_posttype' ); ?></p>
	</section>
</article>
