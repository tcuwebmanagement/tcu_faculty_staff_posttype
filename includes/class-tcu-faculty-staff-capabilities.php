<?php
/**
 * Set user capabilities
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Class to initiate user capabilities
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */
class Tcu_Faculty_Staff_Capabilities {

	/**
	 * Array of capabilities
	 *
	 * @var array List of capabilities.
	 */
	private static $cap_aliases = array(
		// Full permissions to a post type.
		'editor'      => array(
			'read',
			'read_private_posts',
			'edit_posts',
			'edit_others_posts',
			'edit_private_posts',
			'edit_published_posts',
			'delete_posts',
			'delete_others_posts',
			'delete_private_posts',
			'delete_published_posts',
			'publish_posts',
		),
		// Full permissions for content the user created.
		'author'      => array(
			'read',
			'edit_posts',
			'edit_published_posts',
			'delete_posts',
			'delete_published_posts',
			'publish_posts',
		),
		// Create, but not publish.
		'contributor' => array(
			'read',
			'edit_posts',
			'delete_posts',
		),
		// Read only.
		'subscriber'  => array(
			'read',
		),
	);

	/**
	 * Sets up WordPress hooks/actions.
	 */
	public function __construct() {
		// Set up user capabilities.
		add_action( 'wp_loaded', array( $this, 'set_initial_caps' ) );

		// Clear unwanted/old capabilities.
		add_action( 'admin_init', array( $this, 'remove_old_caps' ) );
	}

	/**
	 * Set up user capabilities
	 *
	 * @param string $post_type The post type name.
	 * @param string $role_id The ID for each role.
	 * @param string $level The capability level.
	 *
	 * @return bool True if successful.
	 */
	protected function post_type_caps( $post_type, $role_id, $level = '' ) {

		if ( empty( $level ) ) {
			$level = $role_id;
		}

		if ( 'administrator' === $level ) {
			$level = 'editor';
		}

		if ( ! isset( $this->cap_aliases[ $level ] ) ) {
			return false;
		}

		$role = get_role( $role_id );
		if ( ! $role ) {
			return false;
		}

		$pto = get_post_type_object( Tcu_Faculty_Staff_Main::POSTTYPE );
		if ( empty( $pto ) ) {
			return false;
		}

		foreach ( $this->cap_aliases[ $level ] as $alias ) {
			if ( isset( $pto->cap->$alias ) ) {
				$role->add_cap( $pto->cap->$alias );
			}
		}

		return true;
	}

	/**
	 * Grant caps for the given post type to the given role
	 */
	public function set_initial_caps() {
		foreach ( array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role ) {
			$this->post_type_caps( Tcu_Faculty_Staff_Main::POSTTYPE, $role );
		}
	}

	/**
	 * Remove all caps for the given post type from the given role
	 *
	 * @param string $post_type The post type to remove caps for.
	 * @param string $role_id The role which is losing caps.
	 *
	 * @return bool false if the action failed for some reason, otherwise true.
	 */
	protected function remove_post_type_caps( $post_type, $role_id ) {

		$role = get_role( $role_id );
		if ( ! $role ) {
			return false;
		}

		foreach ( $role->capabilities as $cap => $has ) {
			if ( strpos( $cap, $post_type ) !== false ) {
				$role->remove_cap( $cap );
			}
		}

		return true;
	}

	/**
	 * Remove capabilities for faculty & staff and related post types from default roles
	 */
	public function remove_all_caps() {
		foreach ( array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role ) {
			$this->remove_post_type_caps( Tcu_Faculty_Staff_Main::POSTTYPE, $role );
		}
	}

	/**
	 * Remove old caps that were created from older versions
	 *
	 * An alternative using WP_Roles instead of WP_Role.
	 *
	 * You should call the function when your plugin is activated.
	 *
	 * @uses $wp_roles
	 * @uses WP_Roles::remove_cap()
	 */
	public function remove_old_caps() {
		global $wp_roles;

		// Array of our old capability names.
		$old_caps = array(
			'edit_others_faculty_staff',
			'delete_faculty_staff',
			'delete_others_faculty_staff',
			'read_private_faculty_staff',
			'edit_faculty_staff',
			'delete_faculty_staff',
			'read_faculty_staff',
			'manage_department',
			'edit_department',
			'delete_department',
			'assign_departments',
			'manage_areas_of_study',
			'edit_areas_of_study',
			'delete_areas_of_study',
			'assign_areas_of_study',
			'manage_college_office',
			'edit_college_office',
			'delete_college_office',
			'assign_college_office',
		);

		foreach ( $old_caps as $cap ) {
			foreach ( array_keys( $wp_roles->roles ) as $role ) {
				$wp_roles->remove_cap( $role, $cap );
			}
		}
	}
}
