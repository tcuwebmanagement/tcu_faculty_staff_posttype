<?php
/**
 * Helper classes
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Custom WP_Query
 *
 * @param  array  $args       WP_Query arguments.
 * @param  string $transient  Transient name.
 *
 * @return object $query      The WP_Query.
 */
function tcu_query_posts( $args = array(), $transient = 'tcu_fs_transient_' ) {

	// Our default query arguments.
	$defaults = array(
		'posts_per_page' => -1,
		'post_status'    => 'publish',
		'post_type'      => Tcu_Faculty_Staff_Main::POSTTYPE,
		'no_found_rows'  => true,
	);

	// Merge the query arguments.
	$args = array_merge( (array) $defaults, (array) $args );

	// Check if the transient value exists.
	$query = get_transient( $transient );

	if ( false === $query ) {
		// The value isn't set, so let's create it for a "one day" period.
		$query = new WP_Query( $args );
		set_transient( $transient, $query, 60 * 60 * 24 );
	}

	// Use transient.
	get_transient( $transient );

	return $query;
}

/**
 * Return the WP_Query arguments for a custom loop
 * this depends on the settings page
 *
 * @param  string $orderby ACF name of the orderby selection.
 * @param  string $order   ACF name of the order selection.
 * @param  array  $args    The WP_Query arguments.
 * @return array  $value   The args of the WP_Query to use for ordering.
 */
function tcu_orderby_args( $orderby, $order, $args = array() ) {

	// Get ACF value for options page.
	$value = get_field( $orderby, 'option' );

	switch ( $value ) {

		case 'alpha_name':
			$defaults = array(
				'meta_query' => array(
					'relation'   => 'AND',
					'last_name'  => array(
						'key'     => 'tcu_last_name',
						'compare' => 'EXISTS',
					),
					'first_name' => array(
						'key'     => 'tcu_first_name',
						'compare' => 'EXISTS',
					),
				),
				'orderby'    => array(
					'last_name'  => $order,
					'first_name' => $order,
				),
			);

			// Merge the query arguments.
			$args = array_merge( (array) $defaults, (array) $args );

			break;

		case 'menu_order':
			$defaults = array(
				'orderby' => 'menu_order',
				'order'   => $order,
			);

			// Merge the query arguments.
			$args = array_merge( (array) $defaults, (array) $args );

			break;

		case 'date':
			$defaults = array(
				'orderby' => 'date',
				'order'   => $order,
			);

			// Merge the query arguments.
			$args = array_merge( (array) $defaults, (array) $args );

			break;

		case 'modified':
			$defaults = array(
				'orderby' => 'modified',
				'order'   => $order,
			);

			// Merge the query arguments.
			$args = array_merge( (array) $defaults, (array) $args );

			break;

	}

	return $args;
}

/**
 * Get a list of all our transients
 *
 * @return array $list  List of our transient names.
 */
function tcu_fs_get_all_transients() {
	global $wpdb;

	$transients = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT `option_name` AS `name`, `option_value` AS `value`
            FROM  $wpdb->options
            WHERE `option_name` LIKE %s
            AND `option_name` NOT LIKE %s
			ORDER BY `option_name`",
			'%_fs_transient_%',
			'%_timeout_%'
		)
	);
	$list       = array();

	// Clean up string name.
	foreach ( $transients as $item ) {
		$name = str_replace( '_transient_', '', $item->name );
		array_push( $list, $name );
	}

	return $list;
}

/**
 * Delete transients when a faculty/staff post is saved
 *
 * @param int    $post_id  The post ID.
 * @param object $post     The post object.
 * @param bool   $update   Whether this is an existing post being updated or not.
 *
 * @return null
 */
function tcu_faculty_staff_delete_transient( $post_id, $post, $update ) {
	/*
	 * Get post type slug
	 */
	$post_type = get_post_type( $post_id );

	// Get all our transients.
	$transients = tcu_fs_get_all_transients();

	// Bail if this isn't a 'faculty_staff' post.
	if ( 'faculty_staff' !== $post_type ) {
		return;
	}

	if ( 'faculty_staff' === $post_type ) {

		foreach ( $transients as $name ) {
			delete_transient( $name . '_transient_' );
		}
	}
}

add_action( 'save_post', 'tcu_faculty_staff_delete_transient', 10, 3 );

/**
 * Delete transients when Faculty & Staff settings page is updated
 */
function tcu_clear_transient_settings_page() {

	// Get current screen.
	$screen = get_current_screen();

	// Get all our transients.
	$transients = tcu_fs_get_all_transients();

	if ( strpos( $screen->id, 'acf-options-settings' ) === true ) {
		foreach ( $transients as $name ) {
			delete_transient( $name . '_transient_' );
		}
	}
}

add_action( 'acf/save_post', 'tcu_clear_transient_settings_page', 20 );
