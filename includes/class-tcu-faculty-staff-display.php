<?php
/**
 * Filters and hooks for templates
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Use our templates instead of the ones that come with the theme
 */
class Tcu_Faculty_Staff_Display {

	/**
	 * Sets up WordPress hooks/actions.
	 */
	public function __construct() {
		// Use our own archive template.
		add_filter( 'archive_template', array( $this, 'get_archive_template' ) );

		// User our own single template.
		add_filter( 'single_template', array( $this, 'get_single_template' ) );

		// User our own taxonomy template.
		add_filter( 'taxonomy_template', array( $this, 'get_taxonomy_template' ) );

		// Set up thumbnail.
		add_action( 'init', array( $this, 'set_new_image_size' ) );

		// Remove the archive title prefix.
		add_filter( 'get_the_archive_title', array( $this, 'the_archive_title' ) );

		// Load all our css and js files on front end side.
		add_action( 'wp_enqueue_scripts', array( $this, 'register_front_end_scripts' ) );
	}

	/**
	 * Use our own archive template
	 *
	 * @param string $archive_template   Faculty and staff archive template.
	 * @return string  $archive_template   Full path of the facutly and staff archive template.
	 */
	public function get_archive_template( $archive_template ) {
		global $post;

		if ( is_post_type_archive( Tcu_Faculty_Staff_Main::POSTTYPE ) ) {
			$archive_template = TCU_FACULTY_STAFF_DIRNAME . '/archive-faculty_staff.php';
		}
		return $archive_template;
	}

	/**
	 * Use our own single template
	 *
	 * @param string $single_template   Faculty and staff archive template.
	 * @return string $single_template   Full path of the facutly and staff archive template.
	 */
	public function get_single_template( $single_template ) {
		global $post;

		if ( Tcu_Faculty_Staff_Main::POSTTYPE === $post->post_type ) {
			$single_template = TCU_FACULTY_STAFF_DIRNAME . '/single-faculty_staff.php';
		}

		return $single_template;
	}

	/**
	 * Use our own taxonomy template
	 *
	 * @param string $template   Faculty and staff archive template.
	 * @return string $template   Full path of the facutly and staff archive template.
	 */
	public function get_taxonomy_template( $template ) {
		$taxonomy = get_query_var( 'taxonomy' );

		if ( Tcu_Faculty_Staff_Main::DEPARTMENT === $taxonomy ) {
			$template = TCU_FACULTY_STAFF_DIRNAME . '/taxonomy-department.php';
		}

		if ( Tcu_Faculty_Staff_Main::AREASOFSTUDY === $taxonomy ) {
			$template = TCU_FACULTY_STAFF_DIRNAME . '/taxonomy-areas_of_study.php';
		}

		return $template;
	}

	/**
	 * Set up image size for thumbnail
	 */
	public function set_new_image_size() {
		add_image_size( 'staff_thumbnail', 150, 200, true );
	}

	/**
	 * Remove anything that looks like an archive title prefix ("Archive:", "Foo:", "Bar:").
	 *
	 * @param string $title The post title.
	 */
	public function the_archive_title( $title ) {
		return preg_replace( '/^\w+: /', '', $title );
	}

	/**
	 * Load CSS file
	 */
	public function register_front_end_scripts() {

		wp_register_style( 'tcu-faculty-staff-styles', plugins_url( '/css/styles.min.css', dirname( __FILE__ ) ), array(), Tcu_Faculty_Staff_Main::VERSION, 'all' );

		if ( is_post_type_archive( 'faculty_staff' ) || is_tax( 'department' ) || is_tax( 'areas_of_study' ) ) {
			wp_enqueue_style( 'tcu-faculty-staff-styles' );
		}
	}
}
