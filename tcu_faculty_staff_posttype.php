<?php
/**
 * Plugin Name:    TCU Faculty &amp; Staff Post Type
 * Description:    This plugin adds a Staff Custom Post Type to any TCU theme developed to use for Web Standards. This plugin will only function correctly if it is used with the new Web Standards themes. Your single, archive, and taxonomy page templates will not work correctly.
 * Version:        3.1.0
 * Author:         Website &amp; Social Media Management
 * Author URI:     http://mkc.tcu.edu/web-management.asp
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 3.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'TCU_FACULTY_STAFF_DIRNAME', dirname( __FILE__ ) );

// The main plugin class.
require_once TCU_FACULTY_STAFF_DIRNAME . '/includes/class-tcu-faculty-staff-main.php';

/**
 * Let's get started!
 */
if ( class_exists( 'Tcu_Faculty_Staff_Main' ) ) {
	Tcu_Faculty_Staff_Main::instance();
}
