<?php
/**
 * Display Faculty & Staff by Area of Study Template
 *
 * @package tcu_faculty_staff_posttype
 * @since TCU Faculty Staff Post Type 1.0.0
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The ACF name for the orderby field
 * This option is in the settings page
 */
$tcu_orderby = Tcu_Faculty_Staff_Admin::LEVDEPTAOSORDERBY;

/**
 * The option to order our query by ASC/DESC order
 * This option is located in the settings page
 */
$tcu_order = get_field( Tcu_Faculty_Staff_Admin::LEVDEPTAOSORDER, 'option' );

// Our header.
get_header();

// Display breadcrumbs.
if ( function_exists( 'tcu_breadcrumbs_list' ) ) {
	tcu_breadcrumbs_list();
} ?>

	<div class="tcu-layoutwrap--transparent">

		<div class="tcu-layout-constrain cf">

			<main class="unit size1of1 m-size1of1 main cf" id="main">

				<h1><?php single_cat_title(); ?></h1>

				<!-- Let's begin our table -->
				<table class="tcu-table tcu-table--fs tcu-article__content cf">

				<?php
				/**
				 * Find all our terms
				 */
				$tcu_aos_terms = get_the_terms( get_the_ID(), Tcu_Faculty_Staff_Main::AREASOFSTUDY );

				/**
				 * Filter through Areas of Study terms and grab the current post.
				 * We are going to query through the areas of study terms
				 */
				$tcu_current_aos_term = array_filter( $tcu_aos_terms, function( WP_Term $post ) {
					if ( single_cat_title( '', false ) === $post->name ) {
						return $post;
					}
				} );
				$tcu_key = key( $tcu_current_aos_term );

				/**
				 * Default arguments for WP_Query
				 */
				$tcu_defaults = array(
					'tax_query' => array(
						'include_children' => false,
						array(
							'taxonomy' => Tcu_Faculty_Staff_Main::AREASOFSTUDY,
							'field'    => 'slug',
							'terms'    => $tcu_current_aos_term[ $tcu_key ]->slug,
						),
					),
				);

				/**
				 * Grab args depending on settings page
				 * Determines the order and orderby of the page
				 */
				$tcu_args = tcu_orderby_args( $tcu_orderby, $tcu_order, $tcu_defaults );

				// Let's create our unique transient name.
				$tcu_transient = strtolower( single_cat_title( '', false ) ) . Tcu_Faculty_Staff_Main::TRANSIENTSLUG;

				/**
				 * Start a new WP_Query
				 * with args from settings page
				 */
				$tcu_new_query = tcu_query_posts( $tcu_args, $tcu_transient );

				/*
				 * Start the loop.
				 */
				if ( $tcu_new_query->have_posts() ) :
					while ( $tcu_new_query->have_posts() ) :
						$tcu_new_query->the_post();

						/**
						 * Include table rows
						 */
						include 'partials/staff-table.php';

					// End the loop.
					endwhile;

				else :

					// If no content, include the "No posts found" template.
					include 'partials/content-none.php';

				endif;
				?>

				</table><!-- end of .tcu-table -->

		</main><!-- end of .main -->

	</div><!-- end of .tcu-layout-constrain -->

</div><!-- end .tcu-layoutwrap--transparent -->

<?php get_footer(); ?>
